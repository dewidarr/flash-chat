import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:progress_indicators/progress_indicators.dart';
import 'package:sosta_chat/components/please_wait_screen.dart';
import 'package:sosta_chat/components/round_button.dart';
import 'package:sosta_chat/homeScreens/home_handler.dart';
import 'package:sosta_chat/localiz/applocaliz.dart';
import 'package:toast/toast.dart';

import '../constants.dart';

final _fireStore = Firestore.instance;

class RegistrationScreen extends StatefulWidget {
  static String id = 'register_screen';

  @override
  _RegistrationScreenState createState() => _RegistrationScreenState();
}

class _RegistrationScreenState extends State<RegistrationScreen> {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  String _email;
  String _password;
  String _userName;
  String _uploadedFileURL;
  bool isUserPickedImage = false;
  File _userImage;
  Image _image = Image.asset(
    'images/addimage.png',
    width: 80.0,
    height: 80.0,
  );

  double _showDots = 0.0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 24.0),
        child: Center(
          child: SingleChildScrollView(
            child: SizedBox(
              height: MediaQuery.of(context).size.height,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Flexible(
                    child: Hero(
                      tag: 'logo',
                      child: Container(
                        height: 200.0,
                        child: Image.asset('images/logo.png'),
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      getImage();
                      // Todo add image to storage
                    },
                    child: _image,
                  ),
                  SizedBox(
                    height: 48.0,
                  ),
                  TextField(
                    keyboardType: TextInputType.emailAddress,
                    textAlign: TextAlign.center,
                    onChanged: (value) {
                      _userName = value;
                    },
                    decoration: KTextFieldDecoration.copyWith(
                        hintText: 'Enter your name',
                        hintStyle: TextStyle(color: Colors.grey)),
                    style: TextStyle(color: Colors.black),
                  ),
                  SizedBox(
                    height: 8.0,
                  ),
                  TextField(
                    keyboardType: TextInputType.emailAddress,
                    textAlign: TextAlign.center,
                    onChanged: (value) {
                      _email = value;
                    },
                    decoration: KTextFieldDecoration.copyWith(
                        hintText: AppLocalizations.of(context)
                            .translate('enter_email'),
                        hintStyle: TextStyle(color: Colors.grey)),
                    style: TextStyle(color: Colors.black),
                  ),
                  SizedBox(
                    height: 8.0,
                  ),
                  TextField(
                    obscureText: true,
                    textAlign: TextAlign.center,
                    onChanged: (value) {
                      _password = value;
                    },
                    decoration: KTextFieldDecoration.copyWith(
                        hintText: AppLocalizations.of(context)
                            .translate('enter_password'),
                        hintStyle: TextStyle(color: Colors.grey)),
                    style: TextStyle(color: Colors.black),
                  ),
                  SizedBox(
                    height: 24.0,
                  ),
                  Center(
                    child: SizedBox(
                        height: _showDots,
                        child: JumpingText(
                          AppLocalizations.of(context).translate('please_wait'),
                          style: TextStyle(
                              color: Colors.blueAccent, fontSize: 24.0),
                        )),
                  ),
                  RoundedButton(
                    title: AppLocalizations.of(context).translate('register'),
                    colour: Colors.blueAccent,
                    onPressed: () {
                      if (_userName != null &&
                          _email != null &&
                          _password != null &&
                          isUserPickedImage) {
                        regesterUser();
                      } else {
                        Toast.show(
                            AppLocalizations.of(context)
                                .translate('fill_all_fields'),
                            context,
                            duration: Toast.LENGTH_LONG,
                            gravity: Toast.BOTTOM);
                      }
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void regesterUser() async {
    /*
    * register user then upload image
    * */
    try {
      Navigator.pushNamed(context, PleaseWait.id);
      final user = await _auth.createUserWithEmailAndPassword(
          email: _email, password: _password);
      if (user != null) {
        if (_userImage != null) {
          uploadImage();
        }
      }

    } catch (e) {
      Toast.show(
          AppLocalizations.of(context).translate('log_in_error'), context,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
      print(e);
    }

    setState(() {
      _showDots = 40.0;
    });
  }

  Future getImage() async {
    final files = await FilePicker.getFile(type: FileType.IMAGE);
    if (files != null) {
      _userImage = files;
      setState(() {
        Image image = Image.file(
          files,
          width: 80.0,
          height: 80.0,
        );
        if (image != null) {
          isUserPickedImage = true;
          _image = image;
        }
      });
    }
  }

  void uploadImage() async {
    try {
      StorageReference storageReference =
          FirebaseStorage.instance.ref().child('profiles/${_userImage.path}');
      StorageUploadTask uploadTask = storageReference.putFile(_userImage);
      await uploadTask.onComplete;
      print('File Uploaded');
      storageReference.getDownloadURL().then((fileURL) {
        setState(() {
          _uploadedFileURL = fileURL;
          pushUserInfo();
        });
      });
    } catch (e) {
      print(e);
      Toast.show(
          AppLocalizations.of(context).translate('check_connection'), context,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
    }
  }

  void pushUserInfo() async {
    print(_userName + _email);
    final user = await FirebaseAuth.instance.currentUser();
    if (user.uid != null && _uploadedFileURL != null) {
      try {
        await _fireStore.collection("users").document(user.uid).setData({
          'username': _userName,
          'email': _email,
          'image': _uploadedFileURL
        });
        Navigator.pop(context);
        Navigator.pushNamed(context, HomeHandler.id);
      }catch(e){print(e);}
    }
  }
}
