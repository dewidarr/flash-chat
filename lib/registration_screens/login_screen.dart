import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:progress_indicators/progress_indicators.dart';
import 'package:sosta_chat/components/round_button.dart';
import 'package:sosta_chat/homeScreens/home_handler.dart';
import 'package:sosta_chat/localiz/applocaliz.dart';
import 'package:sosta_chat/homeScreens/chat_screen.dart';
import 'package:toast/toast.dart';

import '../constants.dart';

class LoginScreen extends StatefulWidget {
  static String id = 'login_screen';

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _auth = FirebaseAuth.instance;
  String _email;
  String _password;
  double _showDots =0.0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 24.0),
        child: Center(
          // because of the overflow when input text get focused
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Flexible(
                  child: Hero(
                    tag: 'logo',
                    child: Container(
                      height: 200.0,
                      child: Image.asset('images/logo.png'),
                    ),
                  ),
                ),
                SizedBox(
                  height: 48.0,
                ),
                TextField(
                  textAlign: TextAlign.center,
                  keyboardType: TextInputType.emailAddress,
                  onChanged: (value) {
                    _email = value;
                  },
                  decoration: KTextFieldDecoration.copyWith(
                      hintText: AppLocalizations.of(context).translate('enter_email'),
                      hintStyle: TextStyle(color: Colors.grey)),
                  style: TextStyle(color: Colors.black),
                ),
                SizedBox(
                  height: 8.0,
                ),
                TextField(
                  textAlign: TextAlign.center,
                  obscureText: true,
                  onChanged: (value) {
                    _password = value;
                  },
                  decoration: KTextFieldDecoration.copyWith(
                      hintText:  AppLocalizations.of(context).translate('enter_password'),
                      hintStyle: TextStyle(color: Colors.grey)),
                  style: TextStyle(color: Colors.black /**/),
                ),
                SizedBox(
                  height: 24.0,
                ),
               Center(
                 child: SizedBox(
                   height: _showDots,
                     child: JumpingText(AppLocalizations.of(context).translate('please_wait'),style: TextStyle(color: Colors.blueAccent,fontSize: 24.0),)),
               )
              ,
                RoundedButton(
                  title: AppLocalizations.of(context).translate('log_in'),
                  colour: Colors.blueAccent,
                  onPressed: () async {
                    setState(() {
                      _showDots = 40.0;
                    });
                    try {
                      final loginResult =
                          await _auth.signInWithEmailAndPassword(
                              email: _email, password: _password);
                      if (loginResult != null) {
                        Navigator.pushNamed(context, HomeHandler.id);
                      }
                      setState(() {
                        _showDots = 0.0;
                      });
                    } catch (e) {
                      Toast.show(AppLocalizations.of(context).translate('log_in_error'), context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
                      print(e);
                    }
                  },
                ),
              ],
          ),
        ),
      ),
    );
  }
}
