import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:flutter/material.dart';
import 'package:sosta_chat/components/round_button.dart';
import 'package:sosta_chat/localiz/applocaliz.dart';
import 'package:sosta_chat/registration_screens/registration_screen.dart';

import 'login_screen.dart';

const String Tag = 'WelcomeScreen';

class WelcomeScreen extends StatefulWidget {
  static String id = 'welcome_screen';

  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;
  Animation _animation;
  Animation _logoAnimation;

  @override
  void initState() {
    super.initState();
    welcomScreenAnimation();
  }

  @override
  void dispose() {
    super.dispose();
    print(Tag + 'animation disposed');
    _animationController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: _animation.value,
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 24.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Row(
              children: <Widget>[
                Hero(
                  tag: 'logo',
                  child: Container(
                    child: Image.asset('images/logo.png'),
                    height: _logoAnimation.value * 100.0,
                  ),
                ),
                flashCahtAnimation()
              ],
            ),
            SizedBox(
              height: 48.0,
            ),
            RoundedButton(
              title: AppLocalizations.of(context).translate('log_in'),
              colour: Colors.lightBlueAccent,
              onPressed: () {
                Navigator.pushNamed(context, LoginScreen.id);
              },
            ),
            RoundedButton(
              title: AppLocalizations.of(context).translate('register'),
              colour: Colors.blueAccent,
              onPressed: () {
                Navigator.pushNamed(context, RegistrationScreen.id);
              },
            ),
          ],
        ),
      ),
    );
  }

  Expanded flashCahtAnimation() {
    return Expanded(
      child: SizedBox(
        height: 100.0,
        width: double.maxFinite,
        child: ColorizeAnimatedTextKit(
            onTap: () {
              print("Tap Event");
            },
            text: [
              "SoSta Society",
            ],
            textStyle: TextStyle(fontSize: 40.0, fontFamily: "Horizon"),
            colors: [
              Colors.black,
              Colors.blue,
              Colors.yellow,
              Colors.red,
            ],
            textAlign: TextAlign.start,
            alignment: AlignmentDirectional.centerStart // or Alignment.topLeft
            ),
      ),
    );
  }

  void welcomScreenAnimation() {
    _animationController =
        AnimationController(vsync: this, duration: Duration(seconds: 1));

    _animation = new ColorTween(begin: Colors.teal, end: Colors.white)
        .animate(_animationController);
    _logoAnimation = new CurvedAnimation(
        parent: _animationController, curve: Curves.decelerate);

    _animationController.forward();
    _animationController.addListener(() {
      setState(() {});
      // to update the animation value to home used
    });
  }
}
