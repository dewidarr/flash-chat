import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:sosta_chat/components/please_wait_screen.dart';
import 'package:sosta_chat/homeScreens/home_handler.dart';
import 'package:sosta_chat/homeScreens/chat_screen.dart';
import 'package:sosta_chat/registration_screens/login_screen.dart';
import 'package:sosta_chat/registration_screens/registration_screen.dart';
import 'package:sosta_chat/registration_screens/welcome_screen.dart';


import 'homeScreens/create_posts/creat_post.dart';
import 'localiz/applocaliz.dart';

void main() => runApp(FlashChat());
final _auth = FirebaseAuth.instance;
FirebaseUser _firebaseUser;

class FlashChat extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      supportedLocales: [
        Locale('en', 'US'),
        Locale('ar', ''),
      ],
      localizationsDelegates: [
        AppLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      theme: ThemeData.dark().copyWith(
        textTheme: TextTheme(
          body1: TextStyle(color: Colors.white),
        ),
      ),
      initialRoute: WelcomeScreen.id,
      routes: {
        HomeHandler.id: (context) => HomeHandler(),
        CreatePost.id: (context) => CreatePost(),
        CreatePost.id: (context) => CreatePost(),
        WelcomeScreen.id: (context) => WelcomeScreen(),
        LoginScreen.id: (context) => LoginScreen(),
        ChatScreen.id: (context) => ChatScreen(),
        RegistrationScreen.id: (context) => RegistrationScreen(),
        PleaseWait.id: (context) => PleaseWait()
      },
    );
  }

  checkUser() async{
    final user = await _auth.currentUser();
    if(_auth.currentUser() != null){
      print('main_class${_auth.currentUser()}');
      return ChatScreen.id;
    }else{
      print('Main_class no user');
      return WelcomeScreen.id;
    }
  }
}
