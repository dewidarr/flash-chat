import 'package:flutter/material.dart';

import 'homeScreens/create_posts/category_view.dart';


const kSendButtonTextStyle = TextStyle(
  color: Colors.lightBlueAccent,
  fontWeight: FontWeight.bold,
  fontSize: 18.0,
);

const kMessageTextFieldDecoration = InputDecoration(
  contentPadding:
      EdgeInsets.only(right: 10.0, top: 10.0, bottom: 10.0, left: 8.0),
  border: InputBorder.none,
);

const kMessageContainerDecoration = BoxDecoration(
  border: Border(
    top: BorderSide(color: Colors.lightBlueAccent, width: 2.0),
  ),
);

const kAddTagsDecoration = BoxDecoration(
  border: Border(
    bottom: BorderSide(color: Colors.lightBlueAccent, width: 1.0),
  ),
);

const KTextFieldDecoration = InputDecoration(
  contentPadding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
  border: OutlineInputBorder(
    borderRadius: BorderRadius.all(Radius.circular(10.0)),
  ),
  enabledBorder: OutlineInputBorder(
    borderSide: BorderSide(color: Colors.blueAccent, width: 1.0),
    borderRadius: BorderRadius.all(Radius.circular(10.0)),
  ),
  focusedBorder: OutlineInputBorder(
    borderSide: BorderSide(color: Colors.black87, width: 2.0),
    borderRadius: BorderRadius.all(Radius.circular(10.0)),
  ),
);

const KWhatisHappeningDecoration = InputDecoration(
  hintText: 'What is happening....',
  hintStyle: TextStyle(color: Colors.black54,fontSize: 20.0),
  enabledBorder: UnderlineInputBorder(
    borderSide: BorderSide(color: Colors.black87),
  ),
  focusedBorder: UnderlineInputBorder(
    borderSide: BorderSide(color: Colors.lightBlueAccent),
  ),
);

const List<Itemm> KCategoriesList = <Itemm>[
    Itemm(
       'car_emergency',
        Icon(
          Icons.directions_car,
          color: Color(0xFF167F67),
        )),
    Itemm(
        'missing_person',
        Icon(
          Icons.info,
          color: Color(0xFF167F67),
        )),
    Itemm(
        'need_help',
        Icon(
          Icons.help,
          color: Color(0xFF167F67),
        )),
    Itemm(
        'events_urgent',
        Icon(
          Icons.event,
          color: Color(0xFF167F67),
        )),
  ];


const Kcar_emergency ="car_emergency";
 const Kmissing_person ="missing_person";
 const Kneed_help ="need_help";
 const Kevents_urgent ="events_urgent";
