
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:progress_indicators/progress_indicators.dart';
import 'package:sosta_chat/localiz/applocaliz.dart';

class PleaseWait extends StatefulWidget {
  static final id ='pleas_wait';

  @override
  _PleaseWaitState createState() => _PleaseWaitState();
}

class _PleaseWaitState extends State<PleaseWait> {
  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 8.0,
      child: Padding(
        padding:  EdgeInsets.only(top:30.0),
        child: Column(
          children: <Widget>[
            Text(AppLocalizations.of(context).translate('please_wait_loading'),style: TextStyle(color: Colors.white,fontSize: 20.0),),Spacer(),
            CollectionScaleTransition(
              children: <Widget>[
                Image.asset('images/zombie2.png',width: 50.0,height: 50.0,),
                Image.asset('images/zombie.png',width: 50.0,height: 50.0,),
                Image.asset('images/zombie1.png',width: 50.0,height: 50.0,),
              ],
            ),
            Spacer()
          ],
        ),
      ),
    );
  }
}
