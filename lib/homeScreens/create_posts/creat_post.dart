import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fancy_dialog/FancyAnimation.dart';
import 'package:fancy_dialog/FancyGif.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sosta_chat/constants.dart';
import 'package:sosta_chat/homeScreens/create_posts/push_post.dart';
import 'package:sosta_chat/localiz/applocaliz.dart';
import 'package:sosta_chat/registration_screens/welcome_screen.dart';
import 'package:toast/toast.dart';
import 'package:fancy_dialog/fancy_dialog.dart';

import 'category_view.dart';

String _postText;
File _postImageToUpload;
CategoriesView _categoriesView;
final _databaseReference = Firestore.instance;
FirebaseUser _firebaseUser;
class CreatePost extends StatefulWidget {
static final id = "Creat_post";

  @override
  _CreatePostState createState() => _CreatePostState();
}

class _CreatePostState extends State<CreatePost> {
  Image _selectedImage = Image.asset(
    'images/addimage.png',
    width: 80.0,
    height: 60.0,
  );

  @override
  void initState() {
    super.initState();

  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: null,
        backgroundColor: Colors.lightBlueAccent,
        title: GestureDetector(
          child: Text('Submit Post'),
          onTap: () => checkUserExist(),
        ),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Padding(
              padding:  EdgeInsets.only(right :10.0),
              child: GestureDetector(
                child: Icon(
                  Icons.done,
                  color: Colors.white,size: 25,
                ),
                onTap: (){
                  checkUserExist();
                },
              ),
            ),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: SizedBox(
          /*
          * to make scroll view full height
          * */
          height: MediaQuery.of(context).size.height,
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  userImage(),
                  Text(
                    'Mohamed Dewidar',
                    style: TextStyle(
                        fontSize: 14.0,
                        color: Colors.blueAccent,
                        fontStyle: FontStyle.italic),
                  )
                ],
              ),
              Row(
                children: <Widget>[
                  Spacer(),
                  Padding(
                    padding: EdgeInsets.only(right: 12.0),
                    child: GestureDetector(
                        onTap: getImageFromDevice, child: _selectedImage),
                  )
                ],
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: TextField(
                  onChanged: (value) {
                    _postText = value;
                  },
                  keyboardType: TextInputType.multiline,
                  maxLines: null,
                  style: TextStyle(color: Colors.black54),
                  textAlign: TextAlign.start,
                  decoration: KWhatisHappeningDecoration,
                ),
              ),
             _categoriesView = CategoriesView(),
              addTags(),
            ],
          ),
        ),
      ),
    );
  }




  Widget addTags() {
    return Padding(
      padding:
          const EdgeInsets.only(left: 8.0, right: 8.0, top: 20.0, bottom: 8.0),
      child: Material(
        borderRadius: BorderRadius.all(Radius.circular(8.0)),
        elevation: 10.0,
        color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                style: TextStyle(color: Colors.black87),
                decoration: KWhatisHappeningDecoration.copyWith(
                  hintStyle: TextStyle(fontSize: 16.0, color: Colors.black),
                  hintText: 'Add Tag',
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                'Suggested',
                style: TextStyle(color: Colors.black, fontSize: 18.0),
              ),
            ),
            Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    SuggestedTag(
                      tag: 'Cars',
                    ),
                    SuggestedTag(
                      tag: 'emergency',
                    ),
                    SuggestedTag(
                      tag: 'soical',
                    ),
                  ],
                ),
                Row(
                  children: <Widget>[
                    SuggestedTag(
                      tag: 'inforamtion',
                    ),
                    SuggestedTag(
                      tag: 'Cars',
                    ),
                    SuggestedTag(
                      tag: 'Cars',
                    )
                  ],
                )
              ],
            )
          ],
        ),
      ),
    );
  }

  void getImageFromDevice() async {
    print('creatpost enterd');
    final files = await FilePicker.getFile(type: FileType.IMAGE);
    if (files != null) {
      _postImageToUpload = files;
      setState(() {
        _selectedImage = Image.file(
          files,
          width: 80.0,
          height: 80.0,
        );
      });
    }
  }

  void checkUserExist() async{
    _firebaseUser = await FirebaseAuth.instance.currentUser();
    if(_firebaseUser !=null) {
      publishPost();
    }
    else{
      showDialog(
          context: context,
          builder: (BuildContext context) => FancyDialog(
              title: AppLocalizations.of(context).translate('register_required'),
              descreption: AppLocalizations.of(context).translate('register_required_details'),
              animationType: FancyAnimation.TOP_BOTTOM,
              gifPath: FancyGif.MOVE_FORWARD,
              ok: AppLocalizations.of(context).translate('register'),
              okFun: () {
                Navigator.pushNamed(context, WelcomeScreen.id);
              }
          )
      );

    }

  }


  void publishPost() async {
    if (_postText != null && _categoriesView.selectedCategory != null) {
      PushPost pushPost = new PushPost(
          whatIsHappening: _postText,
          selectCategory: _categoriesView.selectedCategory.name,
          image: _postImageToUpload, state: context);
      pushPost.setup();
    } else {
      Toast.show(
          AppLocalizations.of(context).translate('add_post_field'),
           context,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
    }

  }


}

class SuggestedTag extends StatelessWidget {
  SuggestedTag({@required this.tag});

  final String tag;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: GestureDetector(
        onTap: () {
          print(tag + 'Tag tapped');
        },
        child: Material(
          color: Color(0xFF7177d0),
          elevation: 10.0,
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: <Widget>[
                Text(
                  tag,
                  style: TextStyle(color: Colors.white),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: Icon(
                    Icons.close,
                    color: Colors.white,
                    size: 14.0,
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

Widget userImage() {
  return Padding(
    padding: const EdgeInsets.all(8.0),
    child: new Container(
        height: 50.0,
        width: 50.0,
        child: ClipRRect(
          borderRadius: new BorderRadius.circular(100.0),
          child: Image.asset('images/defaultuserimage.png'),
        )),
  );
}



