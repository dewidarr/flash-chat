
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:sosta_chat/components/please_wait_screen.dart';
import 'package:sosta_chat/homeScreens/home_handler.dart';
import 'package:sosta_chat/localiz/applocaliz.dart';
import 'package:toast/toast.dart';

String _uploadedFileURL;
class PushPost {
  var _databaseReference = Firestore.instance;

   PushPost({@required this.whatIsHappening,@required this.selectCategory,this.tags,this.image,@required this.state});
 final  String whatIsHappening;
 final  String selectCategory;
 final  String tags;
 final  File image;
 final dynamic state;

FirebaseUser  _user;
Future<bool>  setup()async{
  Navigator.pushNamed(state, PleaseWait.id);
   _user = await FirebaseAuth.instance.currentUser();
  if(image != null) {
    uploadImage();
  }else{
    pushPostInfo();
  }

 }

  void uploadImage() async{
    try {
      StorageReference storageReference =
      FirebaseStorage.instance.ref().child('profiles/${image.path}');
      StorageUploadTask uploadTask = storageReference.putFile(image);
      await uploadTask.onComplete;
      print('File Uploaded');
      storageReference.getDownloadURL().then((fileURL) {
          _uploadedFileURL = fileURL;
          pushPostInfo();
      });
    } catch (e) {
      print(e);
      Toast.show(
          AppLocalizations.of(state).translate('check_connection'), state,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
    }

  }

  void pushPostInfo()async {
    try {
      var values ={
        'text': whatIsHappening,
        'category': selectCategory,
        'user_id': _user.uid
      };

      tags != null ? values.addAll({'tags':tags}) : print('tags is empty');
      _uploadedFileURL != null ? values.addAll({'image':_uploadedFileURL}) : print('post have no image');
      DocumentReference ref = await _databaseReference.collection("other_posts")
          .add(values);

      if(ref != null){
        Toast.show(
            AppLocalizations.of(state).translate('pushed_sucess'), state,
            duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
            Navigator.pop(state);
            Navigator.pop(state);
            Navigator.pushNamed(state , HomeHandler.id);
      }else{
        Navigator.pop(state);
        errorToast();
      }

    }catch(e){
      Navigator.pop(state);
      errorToast();
      print(e);
    }
  }

  void errorToast(){
    Toast.show(
        AppLocalizations.of(state).translate('check_connection'), state,
        duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
  }
}