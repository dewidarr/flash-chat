import 'package:flutter/material.dart';
import 'package:sosta_chat/constants.dart';
import 'package:sosta_chat/localiz/applocaliz.dart';


class CategoriesView extends StatefulWidget {
  Itemm _selectedCategory;


  Itemm get selectedCategory => _selectedCategory;



  @override
  _CategoriesViewState createState() => _CategoriesViewState();
}

class _CategoriesViewState extends State<CategoriesView> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 40.0, left: 8.0, right: 8.0),
      child: Theme(
        data: new ThemeData.light(),
        child: Material(
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
          elevation: 10.0,
          color: Colors.white,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: DropdownButton<Itemm>(
                  isExpanded: true,
                  icon: defaultCatigoryIcon(),
                  hint: categoryHint(),
                  value: widget._selectedCategory,
                  onChanged: (Itemm Value) {
                    setState(() {
                      widget._selectedCategory = Value;
                    });
                  },
                  items: KCategoriesList.map((Itemm user_selected) {
                    return DropdownMenuItem<Itemm>(
                      value: user_selected,
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          user_selected.icon,
                          Padding(
                            padding: EdgeInsets.only(left: 8.0),
                            child: Text(
                              AppLocalizations.of(context)
                                  .translate(user_selected.name),
                              style: TextStyle(color: Colors.black),
                            ),
                          ),
                        ],
                      ),
                    );
                  }).toList(),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 8.0, left: 12.0),
                child: Row(
                  children: <Widget>[
                    Text(
                      'Add your current location',
                      style: TextStyle(color: Colors.black54),
                    ),
                    Spacer(),
                    Text(
                      'Add',
                      style: TextStyle(color: Color(0xff9396be)),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 16.0, left: 8.0),
                      child: Icon(
                        Icons.add_location,
                        color: Colors.black54,
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

Widget categoryHint() {
  return Material(
      color: Colors.white,
      child: Row(
        children: <Widget>[
          Text(
            "Select Catigory",
            style: TextStyle(color: Colors.black54),
          ),
          Text(
            ' *',
            style: TextStyle(color: Colors.red, fontSize: 16.0),
          )
        ],
      ));
}

class Itemm {
  const Itemm(this.name, this.icon);

  final String name;
  final Icon icon;
}

Widget defaultCatigoryIcon() {
  return Padding(
    padding: const EdgeInsets.only(right: 8.0),
    child: Material(
        color: Colors.white,
        child: Row(
          children: <Widget>[
            Text(
              'Select',
              style: TextStyle(color: Color(0xff9396be)),
            ),
            Icon(
              Icons.arrow_forward_ios,
              color: Colors.black54,
            ),
          ],
        )),
  );
}
