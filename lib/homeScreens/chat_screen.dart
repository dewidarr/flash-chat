import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:progress_indicators/progress_indicators.dart';
import 'package:sosta_chat/localiz/applocaliz.dart';
import 'package:sosta_chat/registration_screens/welcome_screen.dart';

import '../constants.dart';

final _fireStore = Firestore.instance;
FirebaseUser _currentUser;

class ChatScreen extends StatefulWidget {
  static String id = 'chat_screen';

  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  final _auth = FirebaseAuth.instance;
  final sendFieldController = TextEditingController();

  String _message;

  @override
  void initState() {
    super.initState();
    getCurrentUser();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: null,
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.close),
              onPressed: () {
                if (_currentUser != null) {
                  _auth.signOut();
                  Navigator.pushNamed(context,WelcomeScreen.id);
                }
              }),
        ],
        title: Text('⚡️${AppLocalizations.of(context).translate('chat')}'),
        backgroundColor: Colors.lightBlueAccent,
      ),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            MessagesStreamBuilder(),
            Container(
              decoration: kMessageContainerDecoration,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(left: 10.0,right: 10.0),
                    child: HeartbeatProgressIndicator(
                      duration: Duration(seconds: 2),
                      child: Icon(
                        Icons.tag_faces,
                        color: Colors.blueAccent,
                        size: 16.0,
                      ),
                    ),
                  ),
                  Expanded(
                    child: TextField(
                      controller: sendFieldController,
                      style: TextStyle(color: Colors.blueAccent),
                      onChanged: (value) {
                        _message = value;
                      },
                      decoration: kMessageTextFieldDecoration.copyWith(
                        hintText: AppLocalizations.of(context).translate('typ_Message'),
                          hintStyle: TextStyle(color: Colors.black54)),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(right: 8.0),
                    child: IconButton(
                      onPressed: () {
                        _fireStore.collection('messages').add(
                            {'text': _message, 'sender': _currentUser.email});
                        sendFieldController.clear();
                      },
                      icon: Icon(Icons.send),
                      iconSize: 24.0,
                      color: Colors.black87,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void getCurrentUser() async {
    try {
      final user = await _auth.currentUser();
      if (user != null) {
        _currentUser = user;
        print(_currentUser.email);
      }
    } catch (e) {
      print(e);
    }
  }
}

class MessagesStreamBuilder extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: _fireStore.collection('messages').snapshots(),
      builder: (context, snaps) {
        if (!snaps.hasData) {
          return Center(
            child: JumpingText(
              AppLocalizations.of(context).translate('please_wait'),
              style: TextStyle(color: Colors.blueAccent, fontSize: 24.0),
            ),
          );
        }
        final messages = snaps.data.documents.reversed;
        List<MessagesBubbles> messagesList = [];
        for (var snapshots in messages) {
          final messageData = snapshots.data['text'];
          final messageSender = snapshots.data['sender'];

          messagesList.add(MessagesBubbles(
              messageData: messageData,
              messageSender: messageSender,
              isMe: messageSender == _currentUser.email));
        }
        return Expanded(
            child: ListView(
              reverse: true,
          children: messagesList,
          padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
        ));
      },
    );
  }
}

class MessagesBubbles extends StatelessWidget {
  MessagesBubbles({this.messageData, this.messageSender, bool this.isMe});

  final String messageData;
  final String messageSender;
  final bool isMe;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment:
            isMe ? CrossAxisAlignment.end : CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(1.0),
            child: Text(
              messageSender,
              style: TextStyle(color: Colors.black54, fontSize: 12.0),
            ),
          ),
          Material(
            borderRadius: BorderRadius.only(
                topLeft: isMe ? Radius.circular(6.0): Radius.circular(0.0),
                topRight: isMe ? Radius.circular(0.0) : Radius.circular(6.0),
                bottomLeft: isMe? Radius.circular(6.0): Radius.circular(16.0),
                bottomRight: isMe ? Radius.circular(16.0) : Radius.circular(6.0)),
            color: isMe ? Colors.lightBlueAccent : Colors.white,
            elevation: isMe ? 8.0 : 14.0,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                messageData,
                style: TextStyle(
                    fontSize: 16.0,
                    color: isMe ? Colors.white : Colors.black87),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
