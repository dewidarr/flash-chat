import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:progress_indicators/progress_indicators.dart';
import 'package:sosta_chat/homeScreens/home_posts/post_view.dart';
import 'package:sosta_chat/localiz/applocaliz.dart';

final _fireStore = Firestore.instance;

class POstsBuilder extends StatefulWidget {
  @override
  _POstsBuilderState createState() => _POstsBuilderState();
}

class _POstsBuilderState extends State<POstsBuilder> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: null,
        backgroundColor: Colors.lightBlueAccent,
        title: Text(AppLocalizations.of(context).translate('sosta_society')),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Icon(Icons.filter_list),
          )
        ],
      ),
      body: StreamBuilder<QuerySnapshot>(
        stream: _fireStore.collection('other_posts').snapshots(),
        builder: (context, snaps) {
          if (!snaps.hasData) {
            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Center(
                  child: JumpingText(
                    AppLocalizations.of(context)
                        .translate('please_wait_loading'),
                    style: TextStyle(color: Colors.black54, fontSize: 24.0),
                  ),
                ),
              ],
            );
          }

          final postsDocuments = snaps.data.documents;
          List<PostItemsView> postsList = [];

          for (var postValiues in postsDocuments) {
            final String text = postValiues.data['text'];
            final String image = postValiues.data['image'];
            final String tags = postValiues.data['tags'];
            final String usid = postValiues.data['user_id'];
            final String category = postValiues.data['category'];
            postsList.add(PostItemsView(
              text: text,
              image: image,
              tags: tags,
              userid: usid,
              category: category,
            ));
          }

          return Column(
            children: <Widget>[
              Expanded(
                  child: ListView(
                children: postsList,
                padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
              )),
            ],
          );
        },
      ),
    );
  }
}
