import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:sosta_chat/constants.dart';
import 'package:sosta_chat/localiz/applocaliz.dart';

final _fireStore = Firestore.instance;

class PostItemsView extends StatefulWidget {
  PostItemsView(
      {@required this.text, this.image, this.tags, this.category,@required this.userid});

  final String text;
  final String image;
  final String tags;
  final String category;
  final String userid;
  @override
  _PostItemsViewState createState() => _PostItemsViewState();
}

class _PostItemsViewState extends State<PostItemsView> {
  String userNetworkImage;

  String userNetworkname;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left:4.0,bottom: 2.0),
      child: Material(
        elevation: 2.0,
        borderRadius: BorderRadius.only(topLeft:Radius.circular(4.0),bottomLeft:Radius.circular(4.0) ),
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                Container(child: userNetworkImage != null? setCirculImage() : null,),
                Text(userNetworkname != null ?userNetworkname : '',style: TextStyle(color: Colors.black54),),
                Spacer(),
                  categoryShape(),

                ],),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(widget.text,style: TextStyle(color: Colors.black54),),
              ),
              getPostImage(),
            ],
          ),
        ),
      ),
    );
  }

  @override
  void initState() {
    getUserData(widget.userid);
  }
  void getUserData(String usId)async {
    _fireStore
        .collection("users").document(usId).get().then((DocumentSnapshot ds){
      setState(() {
        userNetworkImage= ds.data['image'];
        userNetworkname = ds.data['username'];
      });

    });

  }

  setCirculImage() {
    return Padding(
      padding:  EdgeInsets.only(right:8.0),
      child: Container(
          width: 40.0,
          height: 40.0,
          decoration: new BoxDecoration(
              shape: BoxShape.circle,
              image: new DecorationImage(
                  fit: BoxFit.fill,
                  image:  NetworkImage(
                      userNetworkImage)
              )
          )),
    );
  }

  Widget categoryShape() {
    Color cateColor;
    IconData cateIcon;
    if(widget.category == Kcar_emergency){
      cateColor = Colors.deepOrange;
      cateIcon = Icons.directions_car;
    }else if(widget.category == Kmissing_person){
       cateColor = Colors.red;
       cateIcon = Icons.info;
    }else if(widget.category == Kneed_help){
      cateColor = Colors.indigoAccent;
      cateIcon = Icons.help;
    }else if(widget.category == Kevents_urgent){
      cateColor = Colors.blueAccent;
      cateIcon = Icons.event;
    }
    return Material(
      color: cateColor,
      borderRadius: BorderRadius.all(Radius.circular(8.0)),
      child: Padding(
        padding: const EdgeInsets.all(4.0),
        child: Row(
          children: <Widget>[
            Icon(cateIcon),
            Text(AppLocalizations.of(context).translate(widget.category),style:TextStyle(color: Colors.black54) ,),
          ],
        ),
      ),
    );

  }

 Widget getPostImage() {
    if(widget.image != null){
      return Container(
        height: 150.0,
        width: double.maxFinite,
        decoration: new BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
            image: new DecorationImage(
                fit: BoxFit.fill,
                image: NetworkImage(widget.image)
            )
        ),
      );
    }else{
      return Container();
    }
 }

}
