import 'package:fancy_bottom_navigation/fancy_bottom_navigation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sosta_chat/homeScreens/chat_screen.dart';

import 'create_posts/creat_post.dart';
import 'home_posts/home_posts.dart';

bool _showAppbar = true;
CreatePost _createPost;
_HomeHandlerState state;

class HomeHandler extends StatefulWidget {
  static String id = 'SostaPostsHome';
  static final List<Widget> widgetOptions = <Widget>[
    POstsBuilder(),
    _createPost = CreatePost(),
    ChatScreen()
  ];

  @override
  _HomeHandlerState createState() => state = _HomeHandlerState();
}

class _HomeHandlerState extends State<HomeHandler> {
  int _selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: HomeHandler.widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: FancyBottomNavigation(
        activeIconColor: Colors.black87,
        barBackgroundColor: Colors.blueAccent,
        tabs: [
          TabData(iconData: Icons.home, title: "Home"),
          TabData(
              iconData: Icons.add,
              title: " Publish ",
              onclick: () {
                print('publish button clicked');
              }),
          TabData(iconData: Icons.chat, title: "Chat")
        ],
        onTabChangedListener: (position) {
          setState(() {
            position != 0 ? _showAppbar = false : true;
            _selectedIndex = position;
          });
        },
      ),
    );
  }
}
